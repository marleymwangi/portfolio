export const projects = [
  {
    title: 'Covid 19 Tracker',
    description: "React Progressive Web App connected to a Restful API serving the covid infection cases information and mapping the data.",
    image: 'assets/tracker.jpg',
    tags: ['React', 'Node', 'Tailwind', 'Restful API'],
    source: 'https://bitbucket.org/marleymwangi/corona',
    visit: 'https://trackerpandemic.netlify.app',
  },
  {
    title: 'Blog',
    description: "Next js application for SEO Optimization hosting a blog connected to a Backend CMS that serves the data using a RESTful API.",
    image: 'assets/blog.jpg',
    tags: ['Next.js', 'Tailwind', 'CMS', 'Restful API'],
    source: 'https://github.com/acousticintel/blogv1',
    visit: 'https://alphatechtest.vercel.app/',
  },
  {
    title: 'Weather App',
    description: "React application using Apollo Client to build a Simple weather app that recieves data from a Graph-Ql Server.",
    image: 'assets/weather.jpg',
    tags: ['React', 'Apollo GraphQl', 'Graph QL'],
    source: 'https://bitbucket.org/marleymwangi/weather-graphql',
    visit: 'https://qlweather.netlify.app/',
  },
  {
    title: 'Registration',
    description: "React application using firebase backend for authentication and data storage. The application handles online sign up for nurses wishing to enroll in an online course.",
    image: 'assets/registration.jpg',
    tags: ['React', 'Firebase'],
    source: '',
    visit: '',
  },
];

export const workExperience = [
  {
    place: 'Acoustic Intelligence Africa',
    title: 'Full Stack Developer',
    time: '2019 - Present'
  },
  {
    place: 'Freelance',
    title: 'Machine Learning Engineer',
    time: '2018 - 2019'
  }
]

export const skills = {
  technologies: [
    'HTML5',
    'CSS3',
    'Sass',
    'JavaScript',
    'Node',
    'Express',
    'Google Cloud Services',
    'Python',
  ],
  frameworks: [
    'React',
    'Progressive Web App',
    'Next js',
    'Redux',
    'Apollo GraphQL',
    'Flask',
    'Tailwind',
    'Bootstrap',
  ],
  databases: [
    'MySQL',
    'MongoDB',
    'Google cloud Firestore/Firebase'
  ],
  tools:[
    'Visual Studio Code',
    'Version Control (Git)'
  ]
}